function tableCreate(parent, cols, rows){
    let tbl = document.createElement('table');

  for(let i = 0; i < rows; i++){
    let tr = tbl.insertRow();

    for(let j = 0; j < cols; j++){
        let td = tr.insertCell();
        const k = j + 1;
        td.appendChild(document.createTextNode(i+1 + "" + k));
        td.onclick= changeColor;
        td.id = "cell";
        td.style.border = '1px solid black';
      }
    }
  parent.appendChild(tbl);
}
function tableRemove(elem){
  elem.removeChild(elem.childNodes[0]);
}

let butt = document.getElementById('btn');
let parent = document.querySelector('#parent');

butt.onclick = function() {
  let elem = document.getElementById('parent');
  if(elem.hasChildNodes()){
    tableRemove(elem);
  }
  const cols = document.getElementById('columnsInput').value;
  const rows = document.getElementById('rowsInput').value;
  tableCreate(parent, cols, rows)
};

function changeColor(e) {
    var evt = window.event || e,                     
    obj = evt.srcElement || evt.target;          
    if (obj.tagName == 'TABLE') return;               
    while (obj.tagName != 'TD') obj = obj.parentNode; 
    obj.className = obj.className ? '' : 'act';
}



